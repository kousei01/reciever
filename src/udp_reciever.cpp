#include "udp_reciever.h"

UDPReciever::UDPReciever(int port)
{
  udp_manager_.Create();
  // udp_manager_.SetEnableBroadcast(true);
  // udp_manager_.Connect(broadcast_ip, port);

  udp_manager_.Bind(port);
  udp_manager_.SetNonBlocking(true);
}

UDPReciever::~UDPReciever()
{
  udp_manager_.Close();
}

int UDPReciever::receive(char* message, int message_length)
{
  return udp_manager_.Receive(message, message_length);
}