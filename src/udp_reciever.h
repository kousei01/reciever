#pragma once

#include "ofxNetwork.h"

class UDPReciever
{
  private:
    ofxUDPManager udp_manager_;

  public:
    // char buffer_[64];
    // int buffer_length_;
    UDPReciever (int port);
    ~UDPReciever ();

    // void send (const char* message, int message_length);
    int receive(char* message, int message_length);
};
